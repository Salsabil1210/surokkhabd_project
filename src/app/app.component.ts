import { Component, Inject, OnInit, Renderer2 } from '@angular/core';
import { DOCUMENT } from '@angular/common';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  constructor(@Inject(DOCUMENT) private document: Document, private renderer2: Renderer2) { }
  title = 'surokkhabd_project';
  toggleExpand() {
    const nav = this.document.getElementById('collapseableNav')
    if(nav?.classList.contains('show'))
      this.renderer2.removeClass(nav, 'show')
    else  
      this.renderer2.addClass(nav, 'show')
  }

}
