import { Component, OnInit,HostListener } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  takeFullView: boolean = false;
  
  constructor() {

    this.onResize()
  }

  ngOnInit(): void {
  }

  @HostListener('window:resize', ['$event'])
  onResize() {
    if(window.innerWidth < 1024) 
      this.takeFullView = true
    else
      this.takeFullView = false
  }

}
